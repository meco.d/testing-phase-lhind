import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class testing_phase {
    String path;
    String column_name;
    String  operation;
    List<List<String>> records = new ArrayList<>();

    
    public String find_min_max(int index){
        ArrayList<Integer> column = new ArrayList<>();
        int result = 0;
        try {
            for(int i = 0; i < records.size(); ++i){                                               //Looping to get the elements of the required column
                column.add(Integer.valueOf(records.get(i).get(index)));
            }
            if (operation.equalsIgnoreCase("MAX")) {  
                result = Collections.max(column);                                                  //Collection.max iterates through the list and find the max/min value
            }
            else if (operation.equalsIgnoreCase("MIN")){
                result = Collections.min(column);
            }else{
                System.out.print("");
            }
            return String.valueOf(result);
        }catch (Exception e){
            return "Not possible to find the " + operation.toLowerCase(Locale.ROOT)                 //Show error if elements are not numerical
            + " value! The values are not numerical! " + e;
        }
    }

    public void parse_cvs() throws IOException {
        int column_index = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {                                                  //Iterating through every line in the file
                String[] values = line.split(";");
                String[] trimmedArray = new String[values.length];
                for (int i = 0; i < values.length; i++) {
                    trimmedArray[i] = values[i].trim();                                               //Trimming every value to eleminate white spaces around
                }
                if (!trimmedArray[0].isEmpty()) {                                                     //Checking if line is empty, if not it will be added to the
                    records.add(Arrays.asList(trimmedArray));                                         //list of records
                }
            }
        }

        try {
            column_index = records.get(0).indexOf(column_name);                                       //Finding the column that will be proccessed
            records.get(column_index);
        }catch (Exception e) {                                                                        //If column does not exists print error and exit the app
            System.out.println("Not possible to find the " + operation.toLowerCase(Locale.ROOT) 
            + " value! The column does not exist!");
            System.exit(1);
        }

        records.remove(0);                                                                            //Removing the header row from the list

        System.out.println(find_min_max(column_index));
    }

    public void read_params(String[] args) throws IOException {                                        //Parameter management
        path = args[0];
        column_name = args[1];
        operation = args[2];
        parse_cvs();
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 3) {                                                                        //Checking if the number of inputs is valid
            testing_phase tp = new testing_phase();
            tp.read_params(args);
        }else{
            System.out.println("The app takes exactly 3 arguments (" + args.length + " given)");
        }
    }
}