# Testing Phase LHIND  

## Description  

This project implements a simple Java application which parses csv files and returns a value (min/max)   
for the given column as a parammeter.  
The bash script in the other hand will automate the execution of the java application to find every   
csv file within a directory, and run the application for both min and max value.  

## Language used  

Java1.8  
Bash  

## Prerequisites  

Java 8 or higher must be installed,   
Must be in a Linux or Unix like system or have BASH installed in Windows   

## How to use  

First we complie the java application using:  
```javac testing_phase.java```  

To run the Java application the parameters must be passed like this:

java MyApp “{filename}” “{column_name}” “{min_max_parameter}”  
```java MyApp “username.csv” “Identifier” “MAX”```

For the bash script:
./script "{folder_path}" "{column_name}"  
```./script "/home/user/Downloads/" "Identifier"```